package com.example.kafkaexample;

import lombok.Data;

@Data
public class MessageRequest {
    private String message;
}
